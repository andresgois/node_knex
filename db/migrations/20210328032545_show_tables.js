
exports.up = function(knex) {
    return knex.schema
    .createTable('users', function (table) {
       table.increments('id_user').primary();
       table.string('first_name', 255).notNullable();
       table.string('last_name', 255).notNullable();
    })
    .createTable('products', function (table) {
       table.increments('id_product').primary();    
       table.decimal('price').notNullable();
       table.string('name', 200).notNullable();
    })
    .createTable('purchasing', function (table) {
        table.increments('id').primary();
        table.integer('user_id').unsigned().notNullable();
        table.integer('product_id').unsigned().notNullable();
        table.integer('amount').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());

        table.foreign('user_id').references('id_user').inTable('users');
        table.foreign('product_id').references('id_product').inTable('products');
     });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("products")
    .dropTable("users");
};
