
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('purchasing').del()
    .then(function () {
      // Inserts seed entries
      return knex('purchasing').insert([
        {
          id: 1,
          user_id: 2,
          product_id: 1,
          amount: 3
        },
        {
          id: 2,
          user_id: 2,
          product_id: 2,
          amount: 5
        },
      ]);
    });
};
