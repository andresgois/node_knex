
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        { 
          id_user: 1, 
          first_name: 'André', 
          last_name: 'Gois'
        },
        { 
          id_user: 2, 
          first_name: 'Priscila', 
          last_name: 'Erica'
        },
        { 
          id_user: 3, 
          first_name: 'Beatriz', 
          last_name: 'Gois'
        },
      ]);
    });
};
