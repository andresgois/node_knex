module.exports = {
    development: {
      client: 'mysql2',
      connection: {
        host : '127.0.0.1',
        user : 'newuser',
        password : 'password',
        database : 'avengers',
        charset: 'utf8'
      },
      migrations: {
        directory: __dirname + '/db/migrations',
      },
      seeds: {
        directory: __dirname + '/db/seeds'
      }
    }
  }