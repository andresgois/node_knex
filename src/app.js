import express from 'express';
import knex from '../db/knex';
const app = express();

app.get('/', (req, res) => {
    res.send('Home');
});

app.get('/users', async (req, res) => {
    const dados = await knex.select('*').from('users')
    res.json({dados});
});

app.listen(3000, () => {
    console.log('Servidor rodando');
});